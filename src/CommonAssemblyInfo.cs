﻿using System.Reflection;

[assembly: AssemblyVersion("2.0")]
[assembly: AssemblyFileVersion("2.0")]
[assembly: AssemblyCopyright("© 2018 Headspring")]
[assembly: AssemblyProduct("Sample Build Scripts")]
[assembly: AssemblyCompany("Headspring")]
[assembly: AssemblyInformationalVersion("2.0-dev")]